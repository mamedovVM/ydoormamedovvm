A.app({
  appName: "YDoor",
  appIcon: "calculator",
  onlyAuthenticated: true,
  menuItems: [
    {
      name: "Клиенты",
      children: [
        {name: "Люди", entityTypeId: "Membership"},
        {name: "Компании", entityTypeId: "Company"},
        {name: "Вход/Выход", entityTypeId: "MemberSignInOut"}
      ]
    },
    {
      name: "Операции",
      entityTypeId: "Transaction",
      icon: "send-o"
    }, {
      name: "Заказы",
      entityTypeId: "Order",
      icon: "shopping-cart"
    },
    {
      name: "Касса",
      entityTypeId: "PointOfSale",
      icon: "calculator"
    },
    {
      name: "Движение Д/С",
      entityTypeId: "MoneyMovement",
      icon: "calculator"
    },
    {
      name: "Справочники",
      children: [
        {name: "Тарифы", entityTypeId: "MemberType"},
        {
          name: "Товары",
          entityTypeId: "Item",
          icon: "cubes"
        },
        {
          name: "Счета",
          entityTypeId: "Account",
          icon: "cubes"
        },
        {
          name: "Основание движения Д/С",
          entityTypeId: "MovementCause",
          icon: "cubes"
        }
      ]
    }
  ],
  entities: function(Fields) {
    return {
      Membership: {
        fields: {
          cardId: Fields.text("Идентификатор карты").required(),
          name: Fields.text("ФИО").required(),
          birthDate: Fields.date("Дата рождения").required(),
          passportScan: Fields.text("Скан паспорта"),
          passportNumber: Fields.text("Серия номер паспорта").masked("99 99 999999"),
          photo: Fields.link("Фото"),
          company: Fields.reference("Компания", "Company"),
          memberType: Fields.fixedReference("Тариф", "MemberType"),
          periodNumber: Fields.integer("Кол-во периодов"),
          validFrom: Fields.date("Дата начала обслуживания"),
          validUntil: Fields.date("Дата окончания обслуживания"),
          phone: Fields.text("Телефон").required(),
          email: Fields.text("Email"),
          tableNumber: Fields.integer("Номер стола"),
          comment: Fields.textarea("Комментарий"),
          signInOut: Fields.relation("Время входа/выхода", "MemberSignInOutTime", "membership"),
          invoices: Fields.relation("Счета", "Invoice", "membership")
        },
        referenceName: "name",
        showInGrid: ['cardId', 'name', 'memberType', 'validUntil', 'phone', 'email'],
        views: {
          MemberSignInOut: {
            customView: 'member-sign-in-out'
          }
        },
        actions: [{
          id: 'pay',
          name: "Оплатить",
          actionTarget: 'single-item',
          perform: function (Crud, Actions) {
            var crud = Crud.actionContextCrud();
            return crud.readEntity(Actions.selectedEntityId()).then(function (membership) {
              return Crud.crudFor('MemberType').readEntity(membership.memberType.id).then(function (memberType) {
              	return Crud.crudFor('Invoice').createEntity({
                  membership: membership,
                  date: new Date(),
                  amount: memberType.price * membership.periodNumber,
                  paid: memberType.price * membership.periodNumber,
                  paidDate: new Date()
                })
              });
            }).then(function () {
              return Actions.refreshResult();
            });
          }
        }]
      },
      MemberType: {
        fields: {
          name: Fields.text("Наименование").required(),
          lastsDays: Fields.integer("Срок действия, дней"),
          price: Fields.money("Стоимость, руб.")
        },
        referenceName: "name"
      },
      Company: {
        fields: {
          name: Fields.text("Наименование").required(),
          businessField: Fields.text("Вид деятельности")
        },
        referenceName: "name"
      },
      MemberSignInOutTime: {
        fields: {
          membership: Fields.reference("ФИО", "Membership"),
          signInTime: Fields.datetime("Время входа"),
          signOutTime: Fields.datetime("Время выхода")
        },
        showInGrid: ['signInTime', 'signOutTime']
      },
      
      Invoice: {
        fields: {
          membership: Fields.reference("Клиент", "Membership"),
          number: Fields.text("Номер"),
          date: Fields.date("Дата").required(),
          amount: Fields.money("Сумма, руб.").required(),          
          account: Fields.fixedReference("Счет", "Account"),
          paid: Fields.money("Оплачено, руб."),
          paidDate: Fields.date("Дата оплаты"),
          remaining: Fields.money("К оплате, руб.").readOnly()
        },
        beforeSave: function (Entity) {
          Entity.remaining = Entity.amount - Entity.paid;
        },
        showInGrid: ['number', 'date', 'amount', 'account', 'paid', 'paidDate', 'remaining']
      },
      Account: {
        fields: {
          name: Fields.text("Наименование"),
          order: Fields.integer("Порядок"),
          totalPaid: Fields.money("Оплачено всего, руб.").computed('sum(invoices.paid)'),
          totalMovement: Fields.money("Движение Д/С, руб.").computed('sum(moneyMovements.amount)'),
          balance: Fields.money("Баланс").readOnly(),
          invoices: Fields.relation("Счета", "Invoice", "account"),
          moneyMovements: Fields.relation("Движение Д/С", "MoneyMovement", "account")
        },
        sorting: [['order', 1]],
        referenceName: 'name',
        beforeSave: function (Entity) {
          Entity.balance = (Entity.totalPaid ? parseInt(Entity.totalPaid) : 0) + (Entity.totalMovement ? parseInt(Entity.totalMovement) : 0);
        }
      },
      MoneyMovement: {
        fields: {
          account: Fields.fixedReference("Счет", "Account"),
          date: Fields.date("Дата"),
          amount: Fields.money("Сумма, руб."),
          cause: Fields.reference("Основание", "MovementCause")
        }
      },
      MovementCause: {
        fields: {
          name: Fields.text("Основание")
        },
        referenceName: "name",
        sorting: [['name', 1]]
      },
      
      Transaction: {
        fields: {
          item: Fields.reference("Товар", "Item"),
          order: Fields.reference("Заказ", "Order"),
          orderItem: Fields.reference("Позиция", "OrderItem"),
          quantity: Fields.integer("Кол-во")
        },
        showInGrid: ['item', 'order', 'quantity']
      },
      Item: {
        fields: {
          name: Fields.text("Наименование"),
          stock: Fields.integer("Остаток").computed('sum(transactions.quantity)'),
          price: Fields.money("Цена"),
          transactions: Fields.relation("Операции", "Transaction", "item")
        },
        referenceName: "name"
      },
      Order: {
        fields: {
          number: Fields.integer("Номер заказа"),
          date: Fields.date("Дата"),
          total: Fields.money("Сумма, руб.").computed('sum(orderItems.finalPrice)'),
          orderItems: Fields.relation("Позиции", "OrderItem", "order")
        },
        beforeSave: function (Entity, Dates, Crud) {
          if (!Entity.date) {
            Entity.date = Dates.nowDate();
          }
          return Crud.crudFor('OrderCounter').find({}).then(function (last) {
            if (!Entity.number) {
              Entity.number = last[0].number;
              return Crud.crudFor('OrderCounter').updateEntity({id: last[0].id, number: last[0].number + 1});  
            }
          })
        },
        beforeDelete: function (Entity, Crud, Q) {
          var crud = Crud.crudFor('OrderItem');
          return crud.find({filtering: {order: Entity.id}}).then(function (items) {
            return Q.all(items.map(function (i) { return crud.deleteEntity(i.id) }));
          });
        },
        referenceName: "number",
        views: {
          PointOfSale: {
            customView: 'pos'
          }
        }
      },
      OrderItem: {
        fields: {
          order: Fields.reference("Заказ", "Order"),
          item: Fields.fixedReference("Товар", "Item").required(),
          quantity: Fields.integer("Кол-во").required(),
          finalPrice: Fields.money("Итого, руб.").readOnly().addToTotalRow()
        },
        showInGrid: ['item', 'quantity', 'finalPrice'],
        beforeSave: function (Crud, Entity) {
          return Crud.crudFor('Item').readEntity(Entity.item.id).then(function (item) {
            Entity.finalPrice = Entity.quantity * item.price;
          })
        },
        afterSave: function (Crud, Entity) {
          var crud = Crud.crudForEntityType('Transaction');
          return removeTransactions(Crud, Entity).then(function () {
            return crud.createEntity({
              order: Entity.order, 
              orderItem: {id: Entity.id},
              item: Entity.item,
              quantity: Entity.quantity * -1
            })
          })
        },
        beforeDelete: function (Crud, Entity) {
          return removeTransactions(Crud, Entity);
        }
      },
      OrderCounter: {
        fields: {
          number: Fields.integer("Counter")
        }
      },
    }
  },
  migrations: function (Migrations) { return [
    {
      name: "demo-records-1",
      operation: Migrations.insert("Item", [
        {id: "1", name: "Snickers", price: 299},
        {id: "2", name: "Coffee", price: 199},
        {id: "3", name: "Tea", price: 99}
      ])
    },
    {
      name: "demo-records-2",
      operation: Migrations.insert("Transaction", [
        {id: "1", item: {id: "1"}, quantity: "50"},
        {id: "2", item: {id: "2"}, quantity: "100"},
        {id: "3", item: {id: "3"}, quantity: "200"}
      ])
    },
    {
      name: "order-counter",
      operation: Migrations.insert("OrderCounter", [
        {id: "2", number: 1}
      ])
    }
  ]}
});

function removeTransactions(Crud, Entity) {
  var crud = Crud.crudForEntityType('Transaction');
  return crud.find({filtering: {orderItem: Entity.id}}).then(function (transactions) {
    if (transactions.length) {
      return crud.deleteEntity(transactions[0].id);
    }
  });
}